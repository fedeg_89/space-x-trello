# Space-X Trello API

## Running Locally

#### 1. Install dependencies
```bash
npm i
```

#### 2. Modify the .env file
Save `.env.example` as `.env`.

#### 3. Start the server
```bash
npm run dev
```
#### 3. Run the tests
In test folder, save `.env.example` as `.env`. Then, run:
```bash
npm run test
```