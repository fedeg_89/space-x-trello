const express = require('express')

const {	notFound } =  require('../interfaces/controller')

const errorResponse = require('./routes/errorResponse')

const app = express()
app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

const cardsRoutes = require('./routes/cardsRoutes')
app.use('/cards', cardsRoutes)

// Catch and send error messages
app.use(errorResponse)

// Not Found
app.use(notFound)



module.exports = app