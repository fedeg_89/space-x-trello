const express = require('express')
const cardsRoutes = express.Router()

const {	cardCreateApi } =  require('../../interfaces/controller')

// cardsRoutes.route('/:id')
// 	.get(clientGetApi)

cardsRoutes.route('/')
	.post(cardCreateApi)

module.exports = cardsRoutes
