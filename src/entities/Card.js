class Card {
	// There are many more params, but I added the most important ones.
	constructor({ name, desc, idList, idMembers, idLabels }) {
		this.name = name
		this.desc = desc
		this.idList = idList
		this.idMembers = idMembers
		this.idLabels = idLabels
	}
}

module.exports = Card