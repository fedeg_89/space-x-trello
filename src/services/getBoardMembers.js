const getBoardMembersMod = ({ http }) => {
	return async function getBoardMembers() {
		const members = await http.get(`/boards/${process.env.BOARD}/members`)
		return members
	}
}

module.exports = getBoardMembersMod