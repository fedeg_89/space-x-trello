const http = require('../interfaces/axiosAdapter')

const getListIdMod = require('./getListId')
const getLabelIdMod = require('./getLabelId')
const postCardMod = require('./postCard')
const getBoardMembersMod = require('./getBoardMembers')

const getListId = getListIdMod({ http })
const getLabelId = getLabelIdMod({ http })
const postCard = postCardMod({ http })
const getBoardMembers = getBoardMembersMod({ http })

module.exports = { getListId, getLabelId, postCard, getBoardMembers }
