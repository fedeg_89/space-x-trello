const getLabelIdMod = ({ http }) => {
	return async function getLabelId (labelName) {
		const labels = await http.get(`/boards/${process.env.BOARD}/labels`)
		if (labelName) {
			const label = labels.find(l => l.name == labelName).id
			return label
		} else {
			return labels
		}
	}
}

module.exports = getLabelIdMod