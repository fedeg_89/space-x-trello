const getListIdMod = ({ http }) => {
	return async function getListId (listName) {
		const lists = await http.get(`/boards/${process.env.BOARD}/lists`)
		const list = lists.find(l => l.name == listName).id
		return list
	}
}

module.exports = getListIdMod