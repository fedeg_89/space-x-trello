const postCardMod = ({ http }) => {
	return async function postCard(card) {
		return await http.post(`/cards`, card)
	}
}

module.exports = postCardMod