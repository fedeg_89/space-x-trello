const getRandom = require('./getRandom')

const generateBugName = () => {
	const randomNameIndex = getRandom(bugsTypes.length - 1, 0)
	const name = bugsTypes[randomNameIndex]
	const number = getRandom(99999, 10000).toString()

	return `BUG-${name}-${number}`
}

module.exports = generateBugName

const bugsTypes = [
	'SDF',
	'ARSM',
	'RMBL',
	'TLLT'
]
