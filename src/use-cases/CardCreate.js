const Base = require('./Base')
const Card = require('../entities/Card')
const CardError = require('../errors/CardError')

class CardCreate extends Base {
	constructor({ makeCardType, postCard }) {
		super()
		this.makeCardType = makeCardType
		this.postCard = postCard
	}
	async validate({ type, title, description, category }) {
		if (!type) throw new CardError('Type is required', 400)

		const card = await this.makeCardType({ type, title, description, category })
		return new Card(card)
		
	}
	async execute(card) {
		try {
			const response = await this.postCard(card)
			return { 
				status: response.status,
				id: response.data.id,
				msg: `Card ${response.data.name} created.`
			}
			
		} catch (error) {
			return error
		}
	}

}

module.exports = CardCreate