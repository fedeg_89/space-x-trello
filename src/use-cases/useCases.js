// Use cases
// const CardGet = require('./cardGet')
const CardCreate = require('./cardCreate')

const makeCardType = require('./helpers/makeCardTypeIndex')
const { postCard } = require('../services/trelloServices')

// const cardGet = new CardGet({ makeCardType, postCard })
const cardCreate = new CardCreate({ makeCardType, postCard })

module.exports = { cardCreate }