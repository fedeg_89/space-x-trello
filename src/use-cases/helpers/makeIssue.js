const CardError = require('../../errors/CardError')

const makeIssueMod = ({ getListId }) => {
	return async function makeIssue({ title, description }) {
		if (!title) throw new CardError('Issue card must have a title', 400)
		if (!description) throw new CardError('Issue card must have a description', 400)

		const idList = await getListId('To Do')
		const issueCard = {
			name: title,
			desc: description,
			idList
		}
		return issueCard

	}
} 
module.exports = makeIssueMod