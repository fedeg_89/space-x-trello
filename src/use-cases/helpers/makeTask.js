const CardError = require('../../errors/CardError')

const makeTaskMod = ({ getListId, getLabelId }) => {
	return async function makeTask({ title, category }) {
		if (!title) throw new CardError('Task card must have a title', 400)
		if (!category) throw new CardError('Task card must have a category', 400)

		const labels = await getLabelId() // Fecth all labels
		const label = labels.find(l => l.name == category)
		if (!label) throw new CardError('Invalid category', 400)
		
		const idList = await getListId('To Do')

		const taskCard = {
			name: title,
			idList,
			idLabels: [label.id]
		}
		return taskCard
	}
} 
module.exports = makeTaskMod