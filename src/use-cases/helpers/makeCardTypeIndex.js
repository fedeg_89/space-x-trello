const { getListId, getLabelId, getBoardMembers } = require('../../services/trelloServices')
const generateBugName = require('../../tools/generateBugName')
const getRandom = require('../../tools/getRandom')

const makeIssue = require('./makeIssue')({ getListId })
const makeBug = require('./makeBug')({ getListId, getLabelId, getBoardMembers, generateBugName, getRandom })
const makeTask = require('./makeTask')({ getListId, getLabelId })

const makeCardTypeMod = require('./makeCardType')

const makeCardType = makeCardTypeMod({ makeBug, makeIssue, makeTask })

module.exports = makeCardType