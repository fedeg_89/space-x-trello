const CardError = require('../../errors/CardError')

const makeBugMod = ({ getListId, getLabelId, getBoardMembers, generateBugName, getRandom }) => {
	return async function makeBug({ description }) {
		if (!description) throw new CardError('Bug card must have a description', 400)

		const idList = await getListId('To Do')
		const members = await getBoardMembers()
		const member = members[getRandom(members.length - 1, 0)].id
		const idLabel = await getLabelId('Bug')

		const bugCard = {
			name: generateBugName(),
			desc: description,
			idList,
			idMembers: [member],
			idLabels: [idLabel]
		}
		return bugCard

	}
} 
module.exports = makeBugMod