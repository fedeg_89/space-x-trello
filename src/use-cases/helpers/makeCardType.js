const makeCardType = ({ makeIssue, makeBug, makeTask }) => {
	return async function cardTyped({ type, title, description, category }) {
		let card
		switch (type) {
			case 'issue':
				card = await makeIssue({ title, description })
				break
			case 'bug':
				card = await makeBug({ description })
				break
			case 'task':
				card = await makeTask({ title, category })
				break
		
			default:
				throw new CardError('Invalid type', 400)
		}
		return card
	}
}

module.exports = makeCardType