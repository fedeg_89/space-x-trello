const { cardGet, cardCreate } = require('../use-cases/useCases')

const cardGetApi = (req, res, next) =>
	cardGet.run(req.params.id)
		.then(result =>res.status(200).send(result))
		.catch(next)

const cardCreateApi = (req, res, next) =>
	cardCreate.run(req.body)
		.then(result =>res.status(201).send(result))
		.catch(next)

const notFound = (req, res) =>
  res.status(404).send({
		statusCode: 400,
		message: 'Not found'
	})
module.exports = { cardGetApi, cardCreateApi, notFound }