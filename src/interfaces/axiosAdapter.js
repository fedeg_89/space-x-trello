const axios = require('axios')

class Axios {
	constructor({ apiUrl, key, token }) {
		axios.defaults.baseURL = apiUrl
		axios.defaults.headers.common['Content-Type'] = 'application/json'
		axios.defaults.params = {}
		axios.defaults.params['key'] = key
		axios.defaults.params['token'] = token
	}

	async get(url) {
		// url += `?key=${process.env.KEY}&token=${process.env.TOKEN}`
		const { data } = await axios.get(url)
		return data
	}

	async post(url, body) {
		const response = await axios.post(url, body)
		return response
	}
}

module.exports = new Axios({
	apiUrl: process.env.APIURL,
	key: process.env.KEY,
	token: process.env.TOKEN
})