const axios = require('axios')
axios.defaults.baseURL = process.env.APIURL
axios.defaults.headers.common['Content-Type'] = 'application/json'
axios.defaults.params = {}
axios.defaults.params['key'] = process.env.KEY
axios.defaults.params['token'] = process.env.TOKEN

const { cardCreate } = require('../../src/use-cases/useCases')
const CardError = require('../../src/errors/CardError')



const { issue, bug, task } = require('../fake-data/cardsExamples.json')
let issueExample, bugExample, taskExample
let createdIds = []

describe('POST CARD', () => {
	beforeEach(() => {
		issueExample = { ...issue }
		bugExample = { ...bug }
		taskExample = { ...task }
	})

	afterAll(() => {
		return Promise.all(
			createdIds.map(id => axios.delete(`/cards/${id}`))
		).then(result => console.log('Done!'))
	})

	it('creates an issue', async () => {
		const response = await cardCreate.run(issue)
		createdIds.push(response.id)
		expect(response.status).toBe(200)
	})

	it('creates a bug', async () => {
		const response = await cardCreate.run(bug)
		createdIds.push(response.id)
		expect(response.status).toBe(200)

		// Check label and member
		const boardLabels = await axios.get(`/boards/${process.env.BOARD}/labels`)
		const bugLabelId = boardLabels.data.find(l => l.name == 'Bug').id

		const card = await axios.get(`/cards/${response.id}`)
		const { labels, idMembers } = card.data
		expect(labels[0].id).toBe(bugLabelId)

		expect(idMembers).toHaveLength(1)
	})

	it('creates a task', async () => {
		const response = await cardCreate.run(task)
		createdIds.push(response.id)
		expect(response.status).toBe(200)

		// Check label
		const boardLabels = await axios.get(`/boards/${process.env.BOARD}/labels`)
		const taskLabelId = boardLabels.data.find(l => l.name == task.category).id

		const card = await axios.get(`/cards/${response.id}`)
		const labels = card.data.labels
		expect(labels[0].id).toBe(taskLabelId)

	})
	
	describe('Validations', () => {
		test('without type', async () => {
			delete issueExample.type
			async function postCard(data) {
				await cardCreate.run(data)
			}
			await expect(postCard(issueExample)).rejects.toThrowError('Type is required')
			await expect(postCard(issueExample)).rejects.toThrowError(CardError)
		})

		describe('issues', () => {
			it('without title', async () => {
				delete issueExample.title
				async function postCard(data) {
					await cardCreate.run(data)
				}
				await expect(postCard(issueExample)).rejects.toThrowError('title')
				await expect(postCard(issueExample)).rejects.toThrowError(CardError)
			})

			it('without description', async () => {
				delete issueExample.description
				async function postCard(data) {
					await cardCreate.run(data)
				}
				await expect(postCard(issueExample)).rejects.toThrowError('description')
				await expect(postCard(issueExample)).rejects.toThrowError(CardError)
			})
		})

		describe('bugs', () => {
			it('without description', async () => {
				delete bugExample.description
				async function postCard(data) {
					await cardCreate.run(data)
				}
				await expect(postCard(bugExample)).rejects.toThrowError('description')
				await expect(postCard(bugExample)).rejects.toThrowError(CardError)
			})
		})

		describe('tasks', () => {
			it('without title', async () => {
				delete taskExample.title
				async function postCard(data) {
					await cardCreate.run(data)
				}
				await expect(postCard(taskExample)).rejects.toThrowError('title')
				await expect(postCard(taskExample)).rejects.toThrowError(CardError)
			})

			it('without category', async () => {
				delete taskExample.category
				async function postCard(data) {
					await cardCreate.run(data)
				}
				await expect(postCard(taskExample)).rejects.toThrowError('category')
				await expect(postCard(taskExample)).rejects.toThrowError(CardError)
			})
		})
	})

})