const app = require ('./src/server/server')

const PORT = process.env.PORT

app.listen(PORT, () => {
	console.log(`Trello-Space-X listening on port ${PORT}`)
})
